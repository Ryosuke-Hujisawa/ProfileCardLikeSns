## Description

This is a small sample code. Create an SNS-like profile card.

<img width="200" alt="ProfileCardLikeSns.png" src="https://qiita-image-store.s3.amazonaws.com/0/135794/27a2dda7-1d6f-3fe3-486f-786576334cfb.png">


## Tech

- bootstrap
